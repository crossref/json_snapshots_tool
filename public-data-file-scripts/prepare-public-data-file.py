import argparse
import gzip
import json
import os
import sys
import tarfile
import time
import logging

from multiprocessing import Pool


def iterate_files(input_path):
    batch = []
    tar = tarfile.open(input_path, "r:gz")
    for member in tar:
        f = tar.extractfile(member)
        if f is not None:
            batch.append((member.name, json.load(f)))
            if len(batch) == 20:
                yield batch
                batch = []
    yield batch


def process(filename, items, output_dir):
    print("File:", filename)
    with gzip.open(
        output_dir + "/" + filename + ".gz", "wt", encoding="utf-8"
    ) as f:
        json.dump(items, f, indent=2)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Remove limited references from the snapshot"
    )
    parser.add_argument(
        "-i", "--input", help="input file or directory", type=str
    )
    parser.add_argument("-o", "--output", help="output directory", type=str)
    parser.add_argument("-m", "--mailto", help="email address", type=str)
    args = parser.parse_args()

    if not args.input or not args.output:
        logging.error(
            "Input file or directory and output directory have to be provided"
        )
        parser.print_help()
        sys.exit(1)
    if not os.path.exists(args.input):
        logging.error("Input file or directory does not exist")
        parser.print_help()
        sys.exit(1)
    if not os.path.exists(args.output):
        logging.error("Output directory does not exist")
        parser.print_help()
        sys.exit(1)

    i = 0
    for batch in iterate_files(args.input):
        i += 1
        print("Batch", batch[0][0], batch[-1][0])
        with Pool(30) as p:
            p.starmap(process, [(f, items, args.output) for f, items in batch])
        if i % 10 == 0:
            time.sleep(120)
