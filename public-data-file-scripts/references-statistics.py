import argparse
import gzip
import json
import os
import requests
import sys
import tarfile

def iterate_files(input_path):
    if os.path.isfile(input_path):
        tar = tarfile.open(input_path, 'r:gz')
        for member in tar:
            f = tar.extractfile(member)
            if f is not None:
                yield member.name, json.load(f)
    else:
        n = 0
        while True:
            fn = str(n) + '.json'
            fp = input_path + '/' + fn + '.gz'
            if not os.path.exists(fp):
                break
            with gzip.open(fp, 'rt') as f:
                yield fn, json.load(f)
            n += 1


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Test snapshot')
    parser.add_argument('-i',
                        '--input',
                        help='input file or directory',
                        type=str)
    args = parser.parse_args()

    if not args.input:
        logging.error(
            'Input file or directory and output directory have to be provided')
        parser.print_help()
        sys.exit(1)
    if not os.path.exists(args.input):
        logging.error('Input file or directory does not exist')
        parser.print_help()
        sys.exit(1)
    
    total_works = 0
    works_with_refs = 0
    total_refs = 0
    for name, items in iterate_files(args.input):
        total_works += len(items['items'])
        for item in items['items']:
            total_refs += len(item.get('reference', []))
            if 'reference' in item:
                works_with_refs += 1
        print(name, total_works, works_with_refs, total_refs)
