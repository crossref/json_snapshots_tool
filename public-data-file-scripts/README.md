# Public Data File

This folder contains Python scripts for creating a Public Data File from a snapshot. The full procedure for creating a Public Data File is as follows:

## 1. Download a snapshot

Download the latest snapshot from [https://api.crossref.org/snapshots](https://api.crossref.org/snapshots). Detailed instructions are [here](https://www.crossref.org/documentation/metadata-plus/metadata-plus-snapshots/).

An example, using wget, to fetch the latest snapshot is below. Replace `<TOKEN>` with your Plus token:

```
wget --header=\"Crossref-Plus-API-Token: Bearer <TOKEN>" https://api.crossref.org/snapshots/monthly/latest/all.json.tar.gz
```

## 2. Prepare the data

Use `prepare-public-data-file.py` to prepare the Public Data File:

```
python3 prepare-public-data-file.py -i <snapshot file> -o <output dir> -m <polite email>
```

The following steps are performed by this script:

* download the list of open reference prefixes from the REST API's /members route
* iterate over the snapshot and remove references from works whose prefix is not in the open reference prefixes list
* store the results in the form of multiple .gz files

Note that it can take hours to complete this step.

## 3. Verification

It is recommended to verify whether limited references were indeed removed before publishing the Public Data File. To do that, use `references-statistics.py`:

```
python3 references-statistics.py -i <snapshot file>
```

```
python3 references-statistics.py -i <Public Data File dir>
```

The script calculates the total number of works with references and the total number of references. After running it for the original snapshot and for the generated Public Data File directory, we can verify that the numbers for the Public Data File are smaller and that indeed some references were removed as part of generating the Public Data File. We can also compare the numbers with the numbers reported by the REST API.

## 4. Publish a torrent

Share the torrent through https://academictorrents.com. Crossref already has an account, password can be found in 1password.

