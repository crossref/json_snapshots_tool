(defproject crossref/json-snapshots "1.0.0"
  :description "Generate JSON snapshots"
  :url "https://gitlab.com/crossref/json_snapshots_tool"
  :jvm-opts ["-Xmx8g" "-Dclojure.core.async.pool-size=4"]
  :main crossref.snapshots.snapshots
  :profiles {:scheduled {:main crossref.snapshots.schedule}
             :dev {:eastwood {:config-files ["eastwood-disabled-warnings.clj"]}}}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [clj-time "0.14.2"]
                 [environ "1.1.0"]
                 [cheshire "5.8.0"] 
                 [clj-http "3.12.3"]
                 [clojurewerkz/quartzite "1.0.1"]
                 [info.hoetzel/clj-nio2 "0.1.1"]
                 [com.taoensso/timbre "5.1.0"]
                 [org.clojure/core.async "0.5.527"]
                 [org.apache.commons/commons-compress "1.8"]
                 [io.sentry/sentry-clj "3.1.138"]
                 [amazonica "0.3.150"]]
  :plugins [[jonase/eastwood "0.3.5"]])
