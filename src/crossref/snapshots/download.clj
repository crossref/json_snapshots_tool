(ns crossref.snapshots.download
  (:require [crossref.snapshots.api :as api]
            [crossref.snapshots.local-storage :as storage]
            [clj-time.core :as time]
            [clj-time.format :as format]
            [clojure.core.async :refer [>! go chan poll!]]
            [nio2.io :refer [path]]
            [sentry-clj.core :as sentry]
            [taoensso.timbre :refer [info error]])
  (:import [java.nio.file Files]))

(def first-deposit-date (time/date-time 2002 7 24))

(def rows-list [1000 800 500 200 100 50 20 10])

(defn time-chunks [last-day]
  (let [ends (iterate #(time/minus % (time/days 1)) last-day)
        starts (map #(time/plus % (time/days 1)) (drop 1 ends))
        ends (take-while (partial time/before? first-deposit-date) ends)
        f (partial format/unparse (format/formatters :year-month-day))]
    (map #(vector (f %1) (f %2)) starts ends)))

(defn download-chunk
  ([chunk]
   (info "Downloading" chunk)
   (download-chunk chunk rows-list))
  ([chunk rows-list]
   (try
     (let [chunk-dir (storage/setup-tmp-chunk-dir chunk)
           rows (first rows-list)
           items (api/items-all (first chunk) (second chunk) rows)
           partitioned (partition 2000 2000 nil items)]
       (dorun (map (partial storage/save-items chunk-dir) (range) partitioned)))
     (catch Exception e
       (sentry/send-event {:throwable e})
       (let [remaining-rows (rest rows-list)
             errStr (.toString e)]
         (if (.contains errStr "org.apache.http.ContentTooLongException")
           (if (empty? remaining-rows)
             (do
               (error "Downloading" chunk "failed")
               (throw e))
             (do
               (.printStackTrace e)
               (error "Downloading" chunk "failed, trying with rows" (first remaining-rows) "...")
               (download-chunk chunk remaining-rows)))
           (do
             (.printStackTrace e)
             (error "Downloading" chunk "failed, retrying...")
             (Thread/sleep 10000)
             (download-chunk chunk rows-list))))))))

(defn download-all [snapshot-month]
  (let [chunks (time-chunks snapshot-month)
        total-items (api/items-total (second (first chunks)))
        ; Chunks are DOI creation dates (1 chunk = 1 day), by which we divide the set of DOIs
        ; into subsets that are downloaded in parallel. In general we want to process the subsets
        ; in order from more to less numerous, to minimize the time when threads are idle.
        ; To achieve this, days are sorted from the newest to the oldest, which usually corresponds to
        ; decreasing number of DOIs. One exception are the first few days in 2002, which contain
        ; millions of DOIs. To fix this, here we move those first few days to the beginning of the list.
        chunks (concat (reverse (take-last 10 chunks)) (drop-last 10 chunks))
        done (atom 0)
        done-chan (chan (count chunks))]
    
    (doseq [chunk chunks]
      (go (download-chunk chunk)
          (>! done-chan chunk)
          (swap! done inc)
          (info "Done " chunk "-" @done "out of" (count chunks))))
    
    (let [[archive-name archive-file] (storage/new-archive)
          seq-num (atom 0)
          total-stored (atom 0)]
      
      (with-open [archive archive-file]
        
        (doseq [_ chunks]
          (let [chunk (atom (poll! done-chan))]
            (while (nil? @chunk)
              (Thread/sleep 10000)
              (reset! chunk (poll! done-chan)))
            (let [[next-seq-num stored] (storage/add-to-archive archive @seq-num @chunk total-items)]
              (reset! seq-num next-seq-num)
              (swap! total-stored + stored))))
        
        (let [[next-seq-num stored] (storage/finish-archive archive @seq-num total-items)]
          (reset! seq-num next-seq-num)
          (swap! total-stored + stored)))
      
      [archive-name {:snapshot-size-bytes (Files/size (path archive-name))
                     :snapshot-files @seq-num
                     :snapshot-dois @total-stored}])))

