(ns crossref.snapshots.local-storage
  (:require [crossref.snapshots.config :as config]
            [cheshire.core :as json]
            [clojure.java.io :as io]
            [nio2.dir-seq :refer [dir-seq-glob]]
            [nio2.io :refer [path]]
            [sentry-clj.core :as sentry]
            [taoensso.timbre :refer [info debug]])
  (:import [java.nio.file Files] 
           [java.io FileOutputStream BufferedOutputStream]
           [java.util.zip GZIPOutputStream]
           [org.apache.commons.compress.archivers.tar TarArchiveOutputStream TarArchiveEntry]  
           [org.apache.commons.compress.utils IOUtils]
           [java.nio.file.attribute FileAttribute]))

(def ^:dynamic *tmp-dir*)

(defn setup-tmp-dir []
  (alter-var-root
    #'*tmp-dir*
    (constantly
      (Files/createTempDirectory (path config/data-dir) "" (into-array FileAttribute [])))))

(defn setup-tmp-chunk-dir [chunk]
  (try
    (let [chunk-dir (io/file (str *tmp-dir* "/" (first chunk) "-" (second chunk)))]
      (.mkdir chunk-dir)
      (doseq [file (.listFiles chunk-dir)]
        (io/delete-file file))
      chunk-dir)
    (catch Exception e
      (sentry/send-event {:throwable e}))))

(defn save-items
  ([dir-path seq-num items]
   (let [file-path (str dir-path "/" seq-num ".json")]
     (with-open [out-file (io/writer file-path)]
       (json/generate-stream {:items items} out-file {:pretty true}))
     (info "Written tmp file" file-path))))

(defn read-items [files]
  (if (empty? files)
    []
    (let [items (with-open [rdr (io/reader (first files))]
                  (-> rdr
                      json/parse-stream
                      (get "items")))]
      (try
        (io/delete-file (first files))
        (catch Exception e
          (sentry/send-event {:throwable e})))
      (lazy-cat items (read-items (rest files))))))

(defn copy-to-archive [in-file seq-num archive]
  (let [entry-name (str seq-num ".json")]
    (with-open [in (io/input-stream in-file)]
      (.putArchiveEntry archive (TarArchiveEntry. in-file entry-name))
      (let [byt (IOUtils/copy in archive)]
        (debug byt "bytes copied to archive"))
      (.closeArchiveEntry archive))
    (info "Archived" entry-name)))

(defn new-archive []
  (let [archive-file (Files/createTempFile (path config/data-dir) "" ".tar.gz" (into-array FileAttribute []))]
    [(str archive-file) (-> archive-file
                            io/file
                            FileOutputStream.
                            BufferedOutputStream.
                            GZIPOutputStream.
                            TarArchiveOutputStream.)]))

(def remaining-items (atom []))

(defn add-to-archive [archive next-seq-num chunk total]
  (let [files (-> (str *tmp-dir* "/" (first chunk) "-" (second chunk))
                  path
                  (dir-seq-glob (str "*.json")))
        items (concat @remaining-items (read-items files))
        partitioned-items (partition config/items-per-file config/items-per-file nil items)
        seq-num (atom next-seq-num)
        stored (atom 0)]
    (doseq [part partitioned-items]
      (if (< (count part) config/items-per-file)
        (reset! remaining-items (into [] part))
        (do
          (save-items *tmp-dir* @seq-num part)
          (let [tmp-file-name (str *tmp-dir* "/" @seq-num ".json")]
            (copy-to-archive (io/file tmp-file-name) @seq-num archive)
            (try
              (io/delete-file tmp-file-name)
              (catch Exception e
                (sentry/send-event {:throwable e}))))
          (info (quot (* 100 config/items-per-file (inc @seq-num)) total) "% archived")
          (swap! seq-num inc)
          (reset! remaining-items [])
          (swap! stored + (count part)))))
    [@seq-num @stored]))

(defn finish-archive [archive seq-num total]
  (save-items *tmp-dir* seq-num @remaining-items)
  (let [tmp-file-name (str *tmp-dir* "/" seq-num ".json")
        remaining-count (count @remaining-items)]
    (copy-to-archive (io/file tmp-file-name) seq-num archive)
    (try
      (io/delete-file tmp-file-name)
      (catch Exception e
        (sentry/send-event {:throwable e})))
    (reset! remaining-items [])
    (info (quot (* 100 config/items-per-file (inc seq-num)) total) "% archived")
    [(inc seq-num) remaining-count]))

(defn delete-dir [file]
  (try
    (when (.isDirectory file)
      (doseq [f (.listFiles file)]
        (delete-dir f)))
    (io/delete-file file)
    (catch Exception e
      (sentry/send-event {:throwable e}))))

(defn cleanup-tmp-files [archive-file stats-file]
  (try
    (delete-dir (io/file *tmp-dir*))
    (io/delete-file (io/file stats-file))
    (io/delete-file (io/file archive-file))
    (catch Exception e
      (sentry/send-event {:throwable e}))))

(defn store-stats [stats]
  (let [tmp-file (Files/createTempFile (path config/data-dir) "" ".json" (into-array FileAttribute []))]
    (with-open [out-file (io/writer tmp-file)]
       (json/generate-stream stats out-file {:pretty true}))
    tmp-file))

