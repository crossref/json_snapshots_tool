(ns crossref.snapshots.validation
  (:require [crossref.snapshots.config :as config]
            [taoensso.timbre :refer [info]]))

(def error-msg
  {:file-size-too-small "Snapshot file is smaller than previous snapshot file"
   :file-size-too-large "Snapshot file is larger than twice the size of previous snapshot file"
   :number-of-dois-too-small "Number of DOIs is smaller than in the previous snapshot file"
   :number-of-dois-too-large "Number of DOIs is larger than twice the number of DOIs in previous snapshot file"
   :number-of-dois-inconsistent "Number of DOIs is inconsistent with number of JSON files"})

(defn validate [stats prev-stats]
  (when (seq prev-stats)
    (info "Validating snapshot...")
    (info "Previous statistics:" prev-stats)
    (info "Current statistics:" stats)
    (let [errors [(when (> (:snapshot-size-bytes prev-stats) (:snapshot-size-bytes stats))
                    :file-size-too-small)
                  (when (< (* 2 (:snapshot-size-bytes prev-stats)) (:snapshot-size-bytes stats))
                    :file-size-too-large)
                  (when (> (:snapshot-dois prev-stats) (:snapshot-dois stats))
                    :number-of-dois-too-small)
                  (when (< (* 2 (:snapshot-dois prev-stats)) (:snapshot-dois stats))
                    :number-of-dois-too-large)
                  (when (or (< (:snapshot-dois stats) (inc (* config/items-per-file (dec (:snapshot-files stats)))))
                            (> (:snapshot-dois stats) (* config/items-per-file (:snapshot-files stats))))
                    :number-of-dois-inconsistent)]]
      (filter identity errors))))

