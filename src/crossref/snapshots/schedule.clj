(ns crossref.snapshots.schedule
  (:require [crossref.snapshots.config :as config]
            [crossref.snapshots.snapshots :as snapshots]
            [clojurewerkz.quartzite.scheduler :as qs]
            [clojurewerkz.quartzite.triggers :as qt]
            [clojurewerkz.quartzite.jobs :as qj :refer [defjob]]
            [clojurewerkz.quartzite.schedule.cron :as cron]
            [taoensso.timbre :as log]))

(def generate-snapshot-trigger
  (qt/build
   (qt/with-identity (qt/key "generate-snapshot-trigger"))
   (qt/with-schedule
     (cron/schedule
      (cron/cron-schedule config/schedule)))))

(defjob generate-snapshot-job [ctx]
  (snapshots/generate-snapshot))

(defn start-snapshots []
  (log/info "Scheduling snapshots for" config/schedule)
  (qs/schedule
   (qj/build
    (qj/of-type generate-snapshot-job)
    (qj/with-identity (qj/key "generate-snapshot")))
   generate-snapshot-trigger))

(def termination (promise))

(defn -main []
  (log/set-level! config/log-level)
  (qs/initialize)
  (qs/start)
  (start-snapshots)
  @termination)

(defn stop []
  (deliver termination true))

