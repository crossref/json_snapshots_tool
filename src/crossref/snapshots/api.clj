(ns crossref.snapshots.api
  (:require [crossref.snapshots.config :as config]
            [cheshire.core :as json]
            [clj-http.client :as client]))

(defn items-total [date]
  (let [request {:accept :json
                 :query-params {"rows" 0
                                "filter" (str "until-created-date:" date)
                                "mailto" config/api-mailto}
                 :headers {"Crossref-Plus-API-Token" (str "Bearer " config/api-plus-token)}
                 :debug true}
        response (client/get config/api-url request)]
    (-> response
        :body
        json/parse-string
        (get-in ["message" "total-results"]))))

(defn items-page [start end rows cursor]
  (let [request {:accept :json
                 :query-params {"rows" rows
                                "cursor" cursor
                                "filter" (str "from-created-date:" start ",until-created-date:" end)
                                "mailto" config/api-mailto}
                 :headers {"Crossref-Plus-API-Token" (str "Bearer " config/api-plus-token)}
                 :debug true}
        response (client/get config/api-url request)]
    (-> response
        :body
        json/parse-string
        (get "message"))))

(defn items-all
  ([start end rows]
   (items-all start end rows "*"))
  ([start end rows cursor]
   (let [response (items-page start end rows cursor)
         items (get response "items")
         next-cursor (get response "next-cursor")]
     (if (empty? items)
       []
       (lazy-cat items (items-all start end rows next-cursor))))))

