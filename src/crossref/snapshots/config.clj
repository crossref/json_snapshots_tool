(ns crossref.snapshots.config
  (:require [environ.core :refer [env]]))

(def api-url (get env :api-url "https://api.staging.crossref.org/works"))
(def api-mailto (env :api-mailto))
(def api-plus-token (env :api-plus-token))

(def items-per-file (Integer/parseInt (get env :items-per-file "5000")))

(def aws-bucket (env :aws-bucket))

(def schedule (get env :schedule "0 0 1 ? * *"))

(def log-level (keyword (get env :log-level "info")))

(def data-dir (get env :data-dir (System/getProperty "java.io.tmpdir")))

(def sentry-dsn (get env :sentry-dsn ""))
(def sentry-env (get env :env "staging"))

(def slack-url (env :slack-url))
