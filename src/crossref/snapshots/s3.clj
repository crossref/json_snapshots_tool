(ns crossref.snapshots.s3
  (:require [crossref.snapshots.config :as config]
            [clojure.java.io :as io]
            [cheshire.core :as json]
            [taoensso.timbre :refer [info debug]]
            [amazonica.aws.s3 :as s3]
            [amazonica.aws.s3transfer :as s3-transfer])
  (:import [com.amazonaws.services.s3.model ObjectTagging Tag]))

(defn upload-file [file-path s3-object & tags]
  (info "Uploading file" file-path "to S3" s3-object "...")
  (let [upl (s3-transfer/upload
              config/aws-bucket
              s3-object
              (io/file file-path))]
    ((:add-progress-listener upl) #(if ((set [:completed :part-completed]) (:event %))
                                     (info "Uploading to S3" %)
                                     (debug "Uploading to S3" %)))
    ((:wait-for-completion upl)))
  (info "Uploaded.")
  (when tags
    (info "Setting tags for " s3-object "to" tags)
    (s3/set-object-tagging
      :bucketName config/aws-bucket
      :key s3-object
      :tagging (ObjectTagging. (map #(Tag. (first %) (second %)) tags)))
    (info "Tags set.")))

(defn upload-failed-snapshot [archive-file stats-file month]
  (upload-file
    archive-file
    (str "failed/" month "/all.json.tar.gz")
    ["Name" "all.json.tar.gz"]
    ["DataSource" "elastic"])
  (upload-file stats-file (str "failed/" month "/statistics.json")))

(defn upload-snapshot [archive-file stats-file month]
  (upload-file
    archive-file
    (str "monthly/" month "/all.json.tar.gz")
    ["Name" "all.json.tar.gz"]
    ["DataSource" "elastic"])
  (upload-file stats-file (str "statistics/" month "/statistics.json")))

(defn get-stats [month]
  (-> (s3/get-object config/aws-bucket (str "statistics/" month "/statistics.json"))
      :input-stream
      slurp
      (json/parse-string true)))

