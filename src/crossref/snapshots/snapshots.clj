(ns crossref.snapshots.snapshots
  (:require [crossref.snapshots.config :as config]
            [crossref.snapshots.download :as download]
            [crossref.snapshots.local-storage :as storage]
            [crossref.snapshots.s3 :as s3]
            [crossref.snapshots.validation :as validation]
            [clojure.string :as str]
            [clj-time.core :as time]
            [clj-time.format :as format]
            [clj-http.client :as client]
            [cheshire.core :as json]
            [environ.core :refer [env]]
            [sentry-clj.core :as sentry]
            [taoensso.timbre :as log]))

(defn current-until-date [month]
  (let [month-str (or month
                      (format/unparse
                        (format/formatters :year-month)
                        (time/minus (time/now) (time/months 1))))]
    (-> month-str
        ((partial format/parse (format/formatters :year-month)))
        (time/plus (time/months 1))
        (time/minus (time/days 1)))))

(defn previous-until-date [month]
  (->> month
       current-until-date
       (format/unparse (format/formatters :year-month))
       (format/parse (format/formatters :year-month))
       (#(time/minus % (time/days 1)))))

(defn date-tag [date]
  (format/unparse (format/formatter "yyyy/MM") date))

(defn slack-report [text]
  (when config/slack-url
    (client/post
      config/slack-url
      {:form-params
       {:payload
        (json/generate-string
          {:text
           (str "JSON snapshot " (date-tag (current-until-date (env :month))) " in " config/sentry-env ": " text)})}})))

(defn generate-snapshot []
  
  (slack-report "started")

  (log/info "Starting...")

  (storage/setup-tmp-dir)
  
  (let [snapshot-until (current-until-date (env :month))
        current-date-tag (date-tag snapshot-until)
        [archive-file stats] (download/download-all snapshot-until)
        stats-file (storage/store-stats (assoc stats :month current-date-tag))
        prev-stats (try
                     (s3/get-stats (date-tag (previous-until-date (env :month))))
                     (catch Exception e
                       (if (str/includes? (.getMessage e) "NoSuchKey")
                         (do
                           (log/error "Error:" (.getMessage e))
                           (.printStackTrace e)
                           (slack-report "previous statistics file missing, will not validate!")
                           (log/info "Previous statistics file missing, will not validate!")
                           nil)
                         (throw e))))
        errors (validation/validate stats prev-stats)]

    (if (seq errors)
      
      (do
        (s3/upload-failed-snapshot archive-file stats-file current-date-tag)
        (log/error "Snapshot validation failed")
        (doseq [err errors]
          (let [err-msg (validation/error-msg err)]
            (log/error "Error:" err-msg)
            (sentry/send-event
              {:throwable
               (Exception. (str "Snapshot for " current-date-tag " failed validation: " err-msg))})
            (slack-report (str "validation failed: " err-msg)))))
      
      (do
        (s3/upload-snapshot archive-file stats-file current-date-tag)
        (slack-report "finished")))
    
    (storage/cleanup-tmp-files archive-file stats-file)))

(defn upload-snapshot-file []
  (let [snapshot-until (current-until-date (env :month))
        current-date-tag (date-tag snapshot-until)]
    (s3/upload-file (env :snapshot-file) (str "monthly/" current-date-tag "/all.json.tar.gz"))))

(defn -main []
  (log/set-level! config/log-level)
  (sentry/init!
    config/sentry-dsn
    {:environment config/sentry-env :release (System/getProperty "json-snapshots.version")})
  (try
    (if (env :snapshot-file)
      (upload-snapshot-file)
      (generate-snapshot))
    (catch Exception e
      (log/error "Error:" (.getMessage e))
      (.printStackTrace e)
      (sentry/send-event {:throwable e})
      (slack-report (str "failed: " (.getMessage e)))
      (System/exit 1)))
  (log/info "Done.")
  (System/exit 0))
