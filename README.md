# JSON snapshots tool

The tool for generating JSON snapshots. The tool downloads all /works metadata from REST API, creates a JSON snapshot (a .tar.gz file) and uploads it to AWS. It contains a scheduler.

## Generating a snapshot

To generate a snapshot, run:

```
docker build -t snapshots .
docker run \
  -e AWS_REGION=<AWS region> \
  -e AWS_ACCESS_KEY_ID=<AWS access key id> \
  -e AWS_SECRET_ACCESS_KEY=<AWS secret access key> \
  -e AWS_BUCKET=<AWS bucket> \
  -e API_MAILTO=<polite email address> \
  -e API_PLUS_TOKEN=<PLUS token> \
  -e SENTRY_DSN=<Sentry DSN> \
  -e SLACK_URL=<Slack Webhook URL> \
  snapshots run
```

## Running scheduler

To run the scheduler service that will generate a snapshot at 6AM on the 1st day of every month:

```
docker build -t snapshots .
docker run \
  -e SCHEDULE="0 0 6 1 * ?"
  -e AWS_REGION=<AWS region> \
  -e AWS_ACCESS_KEY_ID=<AWS access key id> \
  -e AWS_SECRET_ACCESS_KEY=<AWS secret access key> \
  -e AWS_BUCKET=<AWS bucket> \
  -e API_MAILTO=<polite email address> \
  -e API_PLUS_TOKEN=<PLUS token> \
  -e SENTRY_DSN=<Sentry DSN> \
  -e SLACK_URL=<Slack Webhook URL> \
  snapshots with-profile scheduled run
```
