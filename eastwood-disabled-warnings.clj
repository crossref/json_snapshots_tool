(disable-warning
 {:linter :suspicious-expression
  :for-macro 'clojure.core/->
  :if-inside-macroexpansion-of #{'clojurewerkz.quartzite.schedule.cron/schedule}
  :reason "Issue in a third-party library: 'suspicious-expression: -> called with 1 args.  (-> x) always returns x.  Perhaps there are misplaced parentheses?'"})

(disable-warning
 {:linter :wrong-arity
  :function-symbol 'amazonica.aws.s3/set-object-tagging
  :arglists-for-linting '([& args])})

