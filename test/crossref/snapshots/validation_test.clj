(ns crossref.snapshots.validation-test
  (:require [crossref.snapshots.validation :as validation]
            [crossref.snapshots.config :as config]
            [clojure.test :refer [deftest testing is]]))

(deftest validating-snapshot
  
  (with-redefs [config/items-per-file 10]
  
    (testing "validation is passing"
      (is (= [] (validation/validate
                  {:snapshot-size-bytes 100 :snapshot-dois 19 :snapshot-files 2}
                  {:snapshot-size-bytes 100 :snapshot-dois 19 :snapshot-files 2})))
      (is (= [] (validation/validate
                  {:snapshot-size-bytes 167 :snapshot-dois 29 :snapshot-files 3}
                  {:snapshot-size-bytes 100 :snapshot-dois 19 :snapshot-files 2})))
      (is (= [] (validation/validate
                  {:snapshot-size-bytes 200 :snapshot-dois 38 :snapshot-files 4}
                  {:snapshot-size-bytes 100 :snapshot-dois 19 :snapshot-files 2})))
      (is (= [] (validation/validate
                  {:snapshot-size-bytes 100 :snapshot-dois 10 :snapshot-files 1}
                  {:snapshot-size-bytes 100 :snapshot-dois 10 :snapshot-files 1})))
      (is (= [] (validation/validate
                  {:snapshot-size-bytes 100 :snapshot-dois 11 :snapshot-files 2}
                  {:snapshot-size-bytes 100 :snapshot-dois 11 :snapshot-files 2})))
      (is (= [] (validation/validate
                  {:snapshot-size-bytes 100 :snapshot-dois 17 :snapshot-files 2}
                  {:snapshot-size-bytes 100 :snapshot-dois 17 :snapshot-files 2}))))

    (testing "snapshot size is wrong"
      (is (= [:file-size-too-small]
             (validation/validate
               {:snapshot-size-bytes 99 :snapshot-dois 19 :snapshot-files 2}
               {:snapshot-size-bytes 100 :snapshot-dois 19 :snapshot-files 2})))
      (is (= [:file-size-too-large]
             (validation/validate
               {:snapshot-size-bytes 201 :snapshot-dois 19 :snapshot-files 2}
               {:snapshot-size-bytes 100 :snapshot-dois 19 :snapshot-files 2}))))

    (testing "number of DOIs is wrong"
      (is (= [:number-of-dois-too-small]
             (validation/validate
               {:snapshot-size-bytes 100 :snapshot-dois 18 :snapshot-files 2}
               {:snapshot-size-bytes 100 :snapshot-dois 19 :snapshot-files 2})))
      (is (= [:number-of-dois-too-large]
             (validation/validate
               {:snapshot-size-bytes 100 :snapshot-dois 39 :snapshot-files 4}
               {:snapshot-size-bytes 100 :snapshot-dois 19 :snapshot-files 2}))))

    (testing "number of DOIs is inconsistent with number of snapshot files"
      (is (= [:number-of-dois-inconsistent]
             (validation/validate
               {:snapshot-size-bytes 100 :snapshot-dois 11 :snapshot-files 1}
               {:snapshot-size-bytes 100 :snapshot-dois 11 :snapshot-files 1})))
      (is (= [:number-of-dois-inconsistent]
             (validation/validate
               {:snapshot-size-bytes 100 :snapshot-dois 11 :snapshot-files 3}
               {:snapshot-size-bytes 100 :snapshot-dois 11 :snapshot-files 3})))
      (is (= [:number-of-dois-inconsistent]
             (validation/validate
               {:snapshot-size-bytes 100 :snapshot-dois 20 :snapshot-files 1}
               {:snapshot-size-bytes 100 :snapshot-dois 20 :snapshot-files 1})))
      (is (= [:number-of-dois-inconsistent]
             (validation/validate
               {:snapshot-size-bytes 100 :snapshot-dois 20 :snapshot-files 3}
               {:snapshot-size-bytes 100 :snapshot-dois 20 :snapshot-files 3}))))
  
    (testing "error combinations"
      (is (= [:file-size-too-small :number-of-dois-too-small]
             (validation/validate
               {:snapshot-size-bytes 99 :snapshot-dois 15 :snapshot-files 2}
               {:snapshot-size-bytes 100 :snapshot-dois 19 :snapshot-files 2})))
      (is (= [:file-size-too-large :number-of-dois-too-large]
             (validation/validate
               {:snapshot-size-bytes 300 :snapshot-dois 55 :snapshot-files 6}
               {:snapshot-size-bytes 100 :snapshot-dois 19 :snapshot-files 2})))
      (is (= [:file-size-too-small :number-of-dois-too-small :number-of-dois-inconsistent]
             (validation/validate
               {:snapshot-size-bytes 99 :snapshot-dois 15 :snapshot-files 10}
               {:snapshot-size-bytes 100 :snapshot-dois 19 :snapshot-files 2}))))))

