(ns crossref.snapshots.dates-test
  (:require [crossref.snapshots.snapshots :as snapshots]
            [crossref.snapshots.download :as download]
            [clj-time.core :as time]
            [clojure.test :refer [deftest testing is]]))

(deftest last-day-of-month
  
  (testing "last day of the snapshot is corrently calculated"
    (is (= (time/date-time 2013 6 30 0 0 0 0) (snapshots/current-until-date "2013-06")))
    (is (= (time/date-time 2017 2 28 0 0 0 0) (snapshots/current-until-date "2017-02")))

    (time/do-at
      (time/date-time 2018 12 1)
      (is (= (time/date-time 2018 11 30 0 0 0 0) (snapshots/current-until-date nil))))
    (time/do-at
      (time/date-time 2018 1 5)
      (is (= (time/date-time 2017 12 31 0 0 0 0) (snapshots/current-until-date nil))))))

(deftest previous-last-day-of-month

  (testing "last day of the snapshot is corrently calculated"
    (is (= (time/date-time 2013 5 31 0 0 0 0) (snapshots/previous-until-date "2013-06")))
    (is (= (time/date-time 2017 1 31 0 0 0 0) (snapshots/previous-until-date "2017-02")))

    (time/do-at
      (time/date-time 2018 12 1)
      (is (= (time/date-time 2018 10 31 0 0 0 0) (snapshots/previous-until-date nil))))
    (time/do-at
      (time/date-time 2018 1 5)
      (is (= (time/date-time 2017 11 30 0 0 0 0) (snapshots/previous-until-date nil))))))

(deftest calculating-time-chunks
  
  (testing "time chunks are properly calculated"
    (is (= [] (download/time-chunks (time/date-time 2002 7 24))))
    (is (= [["2002-07-25" "2002-07-25"]] (download/time-chunks (time/date-time 2002 7 25))))

    (is (= 31 (count (download/time-chunks (time/date-time 2002 8 24)))))
    (is (= ["2002-08-24" "2002-08-24"] (first (download/time-chunks (time/date-time 2002 8 24)))))
    (is (= ["2002-07-25" "2002-07-25"] (last (download/time-chunks (time/date-time 2002 8 24)))))
    
    (is (= 365 (count (download/time-chunks (time/date-time 2003 7 24)))))
    (is (= ["2003-07-24" "2003-07-24"] (first (download/time-chunks (time/date-time 2003 7 24)))))
    (is (= ["2002-07-25" "2002-07-25"] (last (download/time-chunks (time/date-time 2002 8 24)))))))
    
